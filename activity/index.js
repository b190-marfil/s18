console.log("Hello World");

// ADDITION
function getSum(num1, num2){
	let sum = num1 + num2;
	console.log(sum);
};
console.log("Displayed sum of 5 and 15");
getSum(5,15);

// SUBTRACTION
function getDifference(num1, num2){
	let difference = num1 - num2;
	console.log(difference);
};
console.log("Displayed difference of 20 and 5");
getDifference(20,5);

// MULTIPLICATION
function getProduct(num1, num2){
	return num1 * num2;
};

// DIVISION
function getQuotient(num1, num2){
	return num1 / num2;
};

// Declare product and quotient variables
let product = getProduct(50,10);
let quotient = getQuotient(50,10);

// Print values of product and quotient
console.log("The product of 50 and 10:");
console.log(product);
console.log("The quotient of 50 and 10:");
console.log(quotient);

// Calculate Area of a Circle
function getAreaOfCircle(radius){
	return 3.1416 * (radius ** 2)
};

let circleArea = getAreaOfCircle(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

// Get average of 4 numbers
function getAverage(num1, num2, num3, num4){
	return (num1 + num2 + num3 + num4) / 4;
};
// Print Average
let averageVar = getAverage(20,40,60,80);
console.log("The average of 20, 40, 60, and 80:")
console.log(averageVar);

// Check if passed
function isPassed(score, totalScore){
	let isPassed = (score/totalScore) >= 0.75;
	return isPassed;
};
let isPassingScore = isPassed(38,50);
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);